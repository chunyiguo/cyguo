.. _introduction:


==========================
   Installation
==========================

1. **Prerequisites**:

.. sourcecode:: bash

    # python version > 3.6
    $ python3 -V

    #install jre
    $ sudo apt install default-jre

Install Kafka: https://www.apache.org/dyn/closer.cgi?path=/kafka/2.2.0/kafka_2.12-2.2.0.tgz
    
2. **Clone repo from bitbucket**:

.. sourcecode:: bash

    $ git clone http://bitbucket.iluvatar.ai:7990/scm/csdx/flow-sdk.git

3. **Set virtual environment**:

.. sourcecode:: bash

    
    $ python3 -m venv env
    $ source env/bin/activate
    $ pip install -r core_requirements.txt

``core_requirements.txt`` only includes required packages in flowsdk( not including all packages in examples ):

.. sourcecode:: bash

    # flowsdk needs 13 packages
    Pillow==6.1.0
    aiocontextvars==0.2.2
    av==6.2.0
    faust==1.7.4
    msgpack==0.6.2
    opencv-python==4.1.1.26
    pylib==0.1.16 
    pytest==5.1.2
    pytest-asyncio==0.10.0
    msgpack-numpy==0.4.4.3
    sqlalchemy==1.3.8
    sqlparse==0.3.0
    pandas==0.25.1
 
Install ``pylib`` : http://bitbucket.iluvatar.ai:7990/projects/CSDX/repos/py-lib/browse/doc/use-pylib-HOWTO.md

4. **Install flowsdk package**

.. sourcecode:: bash

    $ pip install -e .

If in vitrual enviroment and find error about javapackage, download jar "spark-streaming-kafka-0-8-assembly_2.11-2.4.3.jar" and copy it to **env/lib/python3.6/site-packages/pyspark/jars**


