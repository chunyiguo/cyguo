.. _examples-batch-connect_with_file_system:

=============================
   Connect with file system
=============================

.. contents::
    :local:
    :depth: 2


Flowsdk is a tool used for data transferring. Besides generating data, it can also get data from existed source(file or database).

This example shows how flowsdk gets data from a file and transfers data. 

Data Flow
------------

.. sourcecode:: bash

       +-------------+
       |    file     |
       +-------------+
             |
             |
             |
             v
    +--------------------------+        
    |     sink(fs2)            |
    |        |                 |
    |        |                 |
    |    output_descriptor     |
    +--------------------------+
             |
             | fsc1(channel)
             |
             v
    +--------------------------+
    |    source(fs1)           |
    |       |                  |
    |       |                  |
    |    input_descriptor      |
    +--------------------------+


DataSink gets data from file, and sends it to DataSource through Kafka channel.

Based on the data flow, create `config.json` file:

.. sourcecode:: bash

    {
      "id": "file-connect-app",
      "kafka": {
        "broker": "kafka://localhost:9092"
      },
      "connector": {
        "source": [
        {
          "name": "fs1",
          "type": "kafka",
          "channel": "fsc1",
          "descriptor": {
            "description": "kafka data source"
          }
        }
        ],
        "sink": [
        {
          "name": "fs2",
          "type": "fs",
          "channel": "fsc1",
          "descriptor": {
            "folder": "your_file_path_here",
            "name": "",
            "description": "file system data sink"
          }
        }
        ]
    },
    "channels": {
      "inputs": {
        "fsc1": {
          "topic": "fs_connect_c1_topic",
          "schema": "{}"
        }
       },
      "outputs": {
        "fsc1": {
          "topic": "fs_connect_c1_topic",
          "schema": "{}"
        }
      }
    }
  }

"id": name of the application

"kafka": Kafka broker's address, if use local kafka, usually uses port 9092

"connector": data connector's config, it has source and sink: sink get data from file
and send to source.

Application
--------------

.. sourcecode:: python

    from flowsdk import Application
    import shutil

    # initialize Application instance
    app = Application()
    csv_file = "./sample.csv"


    @app.task
    async def create_file():
        # create the DataSink from the 'fs2'
        ds = app.create_data_sink('fs2')
        # instantiate the OutputDescriptor of DataSink, 'fs2'
        out = ds.create_output_descriptor()

        # Case 1: manipulate the FileDescriptor with file, str(out)
        # fd = open(str(out), "w+")
        # fd.write("test data is here")
        # fd.close()

        # Case 2: use OutDescriptor, to write data
        # out.write("test data is here ...\n")
        # out.write("2nd line of test data\n")

        # Case 3: copy a csv to the destination, str(out)
        shutil.copy(csv_file, str(out))

        # OutputDescriptor object closed
        await out.close()

        # DataSink close
        await ds.close()


    @app.task
    async def process_file():
        # create the DataSource from the 'fs1'
        ds = app.create_data_source('fs1')
        # instantiate the InputDescriptor of DataSource, 'fs1', blocking operation
        in1 = await ds.open_input_descriptor(destroy=False)

        # access the FileDescriptor file name, str(in1)
        print(f'input file name: {in1}')
        # open the file, str(in1), or pass to the Pandas read_csv()
        fd = open(str(in1), 'r')
        for line in fd:
            print(line)
        fd.close()

        # close the InputDescriptor and manually delete it
        await in1.close()
        in1.delete()

        # close the DataSource
        await ds.close()
        # application exit
        app.exit(1)


    if __name__ == '__main__':
        app.main()


In ``async def create_file()``, when running `create_output_descriptor`, it will create a temporary, empty file under the path you mentioned in config. 
Then you can either write new lines or copy data from csv file into the file.

In ``async def process_file()``, data is already in the input descriptor and easy to retrive. If it's an csv file's address, use pandas and you can get access to whole dataset.

Don't forget to close dataSink and dataSource after using.

   
Starting Kafka
---------------

Before running your app, you need to start Zookeeper and Kafka.

Start Zookeeper first:

.. sourcecode:: console

    $ $KAFKA_HOME/bin/zookeeper-server-start $KAFKA_HOME/etc/kafka/zookeeper.properties

Then start Kafka:

.. sourcecode:: console

    $ $KAFKA_HOME/bin/kafka-server-start $KAFKA_HOME/etc/kafka/server.properties


Running your app
------------------

.. sourcecode:: console
    
    $ python fs_connect.py