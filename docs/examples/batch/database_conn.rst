.. _examples-batch-database_conn:

====================
Database setup tips
====================


1. PostgresSql

    Prerequisties:
    
    .. sourcecode:: python 
                
        #Python version from 3.4 to 3.8
        python3 version

        #PostgreSQL server installed and version from 7.4 to 12
        brew install postgres

        #PostgreSQL client library installed and version from 9.1
        brew install libpq

    Install:

    .. sourcecode:: python 

        pip3 install psycopg2 
    
        # Reference: http://initd.org/psycopg/docs/install.html#install-from-source
    
    Flowsdk connect to PostgreSQL, in config file, add uri: 

    .. sourcecode:: bash
     
        postgresql://[user_name]:[password]@[address]:5432/[db_name]

2. Druid
    
    Install Druid to your local:
    Follow the instructions at: https://druid.apache.org/docs/latest/tutorials/index.html
    
    You can load your own data into druid.

    Flowsdk connect to druid, in config file, add uri: 

    .. sourcecode:: bash
     
        druid://localhost:8082/druid/v2/sql/

3. Cassandra
    
    Install: 

    .. sourcecode:: python 
        
        pip install cql
        pip install cassandra-driver

        brew install cassandra
    
    Create cluster, keyspace and table in Cassandra:
    
    .. sourcecode:: python

        from cassandra import ConsistencyLevel
        from cassandra.cluster import Cluster
        from cassandra.query import SimpleStatement
        KEYSPACE = "test"
    
        def main():
            cluster = Cluster(['127.0.0.1'])
            session = cluster.connect()

            # create keysapce "test"
            session.execute("""
                CREATE KEYSPACE IF NOT EXISTS %s
                WITH replication = { 'class': 'SimpleStrategy', 'replication_factor': '2' }
                """ % KEYSPACE)

            # set keyspace
            session.set_keyspace(KEYSPACE)

            #create table "mytable"
            session.execute("""
                CREATE TABLE IF NOT EXISTS mytable (
                    thekey text,
                    col1 text,
                    col2 text,
                    PRIMARY KEY (thekey, col1)
                )
                """)
          
            # insert data into mytable
            query = SimpleStatement("""
                INSERT INTO mytable (thekey, col1, col2)
                VALUES (%(key)s, %(a)s, %(b)s)
                """, consistency_level=ConsistencyLevel.ONE)

            for i in range(10):
                session.execute(query, dict(key="key%d" % i, a='a', b='b'))

        if __name__ == "__main__":
            main()
    
    
    Reference: https://github.com/datastax/python-driver/blob/master/example_core.py


    Flowsdk connect to Cassandra, in config file, add uri: 

    .. sourcecode:: bash

        cassandra://:@[127.0.0.1]:9042/[keyspace_name]


4. Hive

    Hive server is at 10.150.8.105.
    
    For flowsdk, install pyhive:

    .. sourcecode:: python

        pip install pyhive

    And in config file, specify uri:

    .. sourcecode:: bash
     
        hive://10.150.8.105:10000/[db_name]