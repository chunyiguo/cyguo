.. _examples-batch:

====================
   Batch process
====================

.. toctree::
    :maxdepth: 1
    
    batch_data
    connect_with_file_system
    connect_with_database


Batch data is a group of data. In batch processing, newly arriving data elements are collected into a group. The whole group is then processed at a future time. 


Introduce **data connector** :

    ``Data connector`` is a data provider interface.
    It provides a connection between data source and data processor.
    
    In flowsdk, data connector is a high level defination.
    In specific cases, it can be described as `dataSink` and `dataSource`.
    
    ``DataSink`` builds a connection with file source( like csv file ),
    and create a buffer named "output_descriptor" to store the data. You can put the whole data in the buffer or filter it.

    ``DataSource`` gets data from dataSink and do the real process or send to another processor via Kafka.

    Both stream and batch data can be transferred with data connectors. In this page we only talk about how data connector is used in batch data process.



:ref:`examples-batch-batch_data` has simple examples about how to deal with batch data directly.

:ref:`examples-batch-connect_with_file_system` is an example about getting batch data from file system like a csv file.

:ref:`examples-batch-connect_with_database` is an example about getting batch data from databases.