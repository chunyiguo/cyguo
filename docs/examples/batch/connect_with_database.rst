.. _examples-batch-connect_with_database:

==========================
  Connect with database
==========================

.. contents::
    :local:
    :depth: 2


Connection between flowsdk and database is similar to the connection with file systems. 

This example shows how flowsdk gets data from different types of data sources( PostgreSQL, Durid, sparksql and cassandra ) and transfers data.


Data Flow
------------

.. sourcecode:: bash

       +-------------+
       |  database   |
       +-------------+
             |
             |
             |
             v
    +--------------------------+        
    |      sink                |
    |        |                 |
    |        |                 |
    |    output_descriptor     |
    +--------------------------+
             |
             | dbc1(channel)
             |
             v
    +--------------------------+
    |     source               |
    |       |                  |
    |       |                  |
    |    input_descriptor      |
    +--------------------------+



DataSink has connection with database, when get target data, send it to dataSource via channel. 

Based on the data flow, create `config.json` file:



.. sourcecode:: bash

    {
      "id": "db-connect-app",
      "kafka": {
        "broker": "kafka://localhost:9092"
      },
      "connector": {
        "source": [
          {
            "name": "db2",
            "type": "kafka",
            "channel": "dbc1",
            "descriptor": {
              "description": "kafka data source"
            }
          }
        ],
        "sink": [
          {
            "name": "db1",
            "type": "db",
            "channel": "dbc1",
            "descriptor": {
              "uri": "postgresql+psycopg2://postgres:pass@192.168.161.131:5432/postgres",
              "name": "postgres-prod",
              "packages": [
                {
                  "name": "psycopg2"
                }
              ],
              "description": "database sink"
            }
          },
          {
            "name": "druid",
            "type": "db",
            "channel": "dbc1",
            "descriptor": {
              "uri": "druid://localhost:8082/druid/v2/sql/",
              "name": "druid-dev",
              "packages": [
                {
                  "name": "pydruid"
                }
              ],
              "description": "database sink"
            }
          },
          {
            "name": "sparksql",
            "type": "db",
            "channel": "dbc1",
            "descriptor": {
              "uri": "hive://10.150.8.105:10000/sample",
              "name": "sparksql-qa1",
              "packages": [
                {
                  "name": "hive"
                },
                {
                  "name": "pyhive[hive]"
                }
              ],
              "description": "database sink"
            }
          },
          {
            "name": "cassandra1",
            "type": "cassandra",
            "channel": "dbc1",
            "descriptor": {
              "name": "cassandra-dev",
              "uri": "cassandra://:@[127.0.0.1]:9042/test",
              "packages": [
                {
                  "name": "python-cassandra"
                }
              ],
              "description": "cassandra NoSQL database sink"
            }
          }
        ]
      },
      "channels": {
        "inputs": {
          "dbc1": {
            "topic": "db_connect_c1_topic",
            "schema": "{}"
          }
        },
        "outputs": {
          "dbc1": {
            "topic": "db_connect_c1_topic",
            "schema": "{}"
          }
        }
      }
    }





``id``: name of the application

``kafka``: Kafka broker's address and port

``channels``: channels connect with two blocks, one channel has input and output channels, they have same name and topic

``connector``: data connector between data and kafka, it has source and sink:
    
- **source**: 

``name``: data source's name

``type``: data source's type, always is "kafka"

``channel``: channel name connect to data sink
           
- **sink**: 

``name``: data sink's name

``type``: data sink's type, in this example, it should be "db" as database

``channel``: channel name connect to data source

``descriptor``: details about the database connected, 
           
  ``name``: name of the database

  ``packages``: name of the packages that python need to import

  ``description`` (optional): description of the database
            
  ``uri`` **(very important!)**: locator of the database

    PostgreSQL: "postgresql+psycopg2://[user_name]:[password]@[server_address]:5432/[db_name]"

    Durid: "druid://[server_address]:8082/druid/v2/sql/"

    Sparksql: "hive://10.150.8.105:10000/[db_name]"

    Cassandra: "cassandra://:@[127.0.0.1]:9042/[keyspaces_name]"

    :ref:`examples-batch-database_conn` shows how to set up databse
                       
   




Application
------------------

.. sourcecode:: python
    
    from flowsdk import Application

    # initialize Application instance
    app = Application()


    @app.task
    async def generator_db():
        cassandra1 = app.create_data_sink('cassandra1')
        cs_db = cassandra1.create_output_descriptor()
        await cs_db.write("SELECT * from emp")
        await cs_db.close()
        await cassandra1.close()
    
        # create the DataSink from the 'db1'
        db1 = app.create_data_sink('db1')
        # instantiate the DatabaseConnection of DataSink, 'db1'
        db1_engine = db1.create_output_descriptor()
    
        # results = db_engine.connect().execute("select * from person")
        # for row in results:
        #     print('{} {}'.format(row['first_name'], row['second_name']))
        db1_engine.connect().execute("delete from person where first_name='Ken'")
        db1_engine.connect().execute("insert into person (first_name, second_name, age, address) values ('Ken', 'Doe', 38, 'fdjasl')")

        # write the query
        await db1_engine.write("select * from person")

        # DatabaseConnection object closed
        await db1_engine.close()

        # DataSink close
        await db1.close()

        druid = app.create_data_sink('druid')
        db2_engine = druid.create_output_descriptor()

        await db2_engine.write("SELECT page, COUNT(*) AS Edits FROM tutorial WHERE TIMESTAMP '2015-09-12 00:00:00' <= \"__time\" AND \"__time\" < TIMESTAMP '2015-09-13 00:00:00' GROUP BY page ORDER BY Edits DESC LIMIT 10")

        await db2_engine.close()
        await druid.close()

        sparksql = app.create_data_sink('sparksql')
        db3_engine = sparksql.create_output_descriptor()
        
        await db3_engine.write("select * from dept")

        await db1_engine.close()
        await db2_engine.close()
        await db3_engine.close()
        await sparksql.close()


    @app.task
    async def processor_db():
        # create the DataSource from the 'db2'
        db2 = app.create_data_source('db2')

        while True:
            # instantiate the QueryConnection of DataSource, 'db2', blocking operation
            in1 = await db2.open_input_descriptor()
            if in1 is None:
                break

            # results = await in1.read()
            print(f'+++++received query: {in1.get_query()}')
            # results = in1.connect().execute(in1.get_query())
            # for row in results:
            #     print(f'--{row}')
            df = in1.read_sql_dataframe()
            print(df.head())
    
            await in1.close()

        # close the DataSource
        await db2.close()
        # application exit
        app.exit(1)


    if __name__ == '__main__':
        app.main()

    

When create output_descriptor in DataSink, it will create a database connection. You can do some database related work: write query, select target data, insert new rows, delete useless rows.

After filtering data from database, target data will be get from dataSource and be used in next processes.

Don't forget to close dataSink and dataSource after using.

   
Starting Kafka
---------------

Before running your app, you need to start Zookeeper and Kafka.

Start Zookeeper first:

.. sourcecode:: console

    $ $KAFKA_HOME/bin/zookeeper-server-start $KAFKA_HOME/etc/kafka/zookeeper.properties

Then start Kafka:

.. sourcecode:: console

    $ $KAFKA_HOME/bin/kafka-server-start $KAFKA_HOME/etc/kafka/server.properties


Running your app
------------------

.. sourcecode:: console
    
    $ python db_connect.py