.. _examples-batch-batch_data:

==============
  Batch Data
==============

.. contents::
    :local:
    :depth: 2

This example describes how batch data be generated and sent to processor directly using Kafka.


Data Flow
------------

.. sourcecode:: bash


    +--------------------------+        
    |      sink                |
    |                          |
    +--------------------------+
             |
             | c1(channel)
             |
             v
    +--------------------------+
    |     source               |
    |                          |
    +--------------------------+

It's a very simple data flow, data will be generated in `sink`, then will be sent to `source` via channel.

Based on the data flow, create `config.json` file:

.. sourcecode:: bash

    {
      "id": "batch-data-in-kafka",
      "kafka": {
        "broker": "kafka://localhost:9092"
      },
      "connector": {
        "source": [
          {
            "name": "ds1",
            "type": "kafka",
            "channel": "c1",
            "descriptor": {
              "description": "kafka data source"
            }
          }
        ],
        "sink": [
          {
            "name": "ds2",
            "type": "kafka",
            "channel": "c1",
            "descriptor": {
              "description": "kafka data sink"
            }
          }
        ]
      },
      "channels": {
        "inputs": {
          "c1": {
            "topic": "batch_c1_topic",
            "schema": "{}"
          }
        },
        "outputs": {
          "c1": {
            "topic": "batch_c1_topic",
            "schema": "{}"
          }
        }
      }
    }

Besides channels, we also write connectors in config.json file. 

A connector has a sink and a source, and they have different names. 
But as the flow shows, a channel connects sink and source, so the channel
will be same if these two sink and source ara used as a connector. 

Application
------------------

.. sourcecode:: python

    import asyncio
    from flowsdk import Application
    from flowsdk.constants import DEFAULT_CONFIG
    from pylib.config import init_config, read_config
    from pylib.data.types import Value


    # initialize Application instance
    config = read_config(skip=0, app_dir='./config/kafka-test')
    init_config(config, None, DEFAULT_CONFIG)
    app = Application(config=config)


    @app.task
    async def generator():
        ds = app.create_data_sink('ds2')

        for loop in range (5):
            for index in range(10):
                index += 1
                await ds.write(data=Value(index=index))

            await ds.flush()
            print(f'#####completed to send 10 data at {loop+1}')
            await asyncio.sleep(1)

        await ds.close()


    @app.task
    async def processor():
        ds = app.create_data_source('ds1')
        index = 1
        while True:
            data = await ds.read()
            if data is None:
                break
            print(f'+++++received data: {len(data)} at {index}')
            index += 1

        await ds.close()
        app.exit()


    if __name__ == '__main__':
        """
            start the application main()
        """
        app.main()
    

In `generator()`, create data sink, and write 5 groups, 10 data a group in ds, send to kafka. After sending all data, close the dataSink.

In `processor()`, data source will get data from kafka channel and print. When get all data, close dataSource and stop the app.


Starting Kafka
---------------

Before running your app, you need to start Zookeeper and Kafka.

Start Zookeeper first:

.. sourcecode:: console

    $ $KAFKA_HOME/bin/zookeeper-server-start $KAFKA_HOME/etc/kafka/zookeeper.properties

Then start Kafka:

.. sourcecode:: console

    $ $KAFKA_HOME/bin/kafka-server-start $KAFKA_HOME/etc/kafka/server.properties


Running your app
------------------

.. sourcecode:: console
    
    $ python generate-batch-data_kafka.py