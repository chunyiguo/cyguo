.. _examples-stream-cm_example:

====================
   CM Sample
====================

.. contents::
    :local:
    :depth: 1

Cm_samples describes some basic data flows with strategies and shows how to transfer data.

Condiser there are not only one data source, and data are sent into different kafka channels. 

How the processor get data from correct channel.

So in flowsdk, we have `strategies`: 

    ``Arbitrary``: choose values in any available channel
    
    ``OrderedByTime``: order events received at all channels by their times
        
    ``Pairwise``: pair events, one from each channel, into tuples
        
    ``RoundRobin``: choose events from the channels one by one

Strategies help processor collect data regularly.


Data Flow
-----------

.. sourcecode:: bash


       +---------------+
       |   Generator   |
       +-+----------+--+
         |          |
     +---+          +--+
     |                 |
     | c1              | c2
     v                 v
  +-----+           +-----+
  | CM1 |           | CM2 |
  +--+--+           +--+--+
     |                 |
     | c3              | c4
     |                 |          
     +-----+     +-----+
           |     | one of strategies
           v     v
         +---------+
         |   CM3   |
         +---------+


'Generator': send data to c1, c2 channels

'CM1', 'CM2': process incoming stream data, and send to down-stream channel.   

'CM3': collect stream data from ['c3', 'c4'] based on one strategy

Based on the data flow, create `config.json` file:

.. sourcecode:: 


    {
      "id": "cm-sample",
      "kafka": {
        "broker": "kafka://localhost:9092"
      },
      "channels": {
        "inputs": {
          "c1": {
            "topic": "cm_c1_topic",
            "schema": "{}"
          },
          "c2": {
            "topic": "cm_c2_topic",
            "schema": "{}"
          },
          "c3": {
            "topic": "cm_c3_topic",
            "schema": "{}"
          },
          "c4": {
            "topic": "cm_c4_topic",
            "schema": "{}"
          }
        },
        "outputs": {
          "c1": {
            "topic": "cm_c1_topic",
            "schema": "{}"
          },
          "c2": {
            "topic": "cm_c2_topic",
            "schema": "{}"
          },
          "c3": {
            "topic": "cm_c3_topic",
            "schema": "{}"
          },
          "c4": {
            "topic": "cm_c4_topic",
            "schema": "{}"
          }
        }
      }
    }


Assign Kafka broker's address first, if use local kafka, it usually uses port 9092.

A channel connects two blocks, topic is the name of channel. A channel is divided into input 
and output, they have same topic.
     


Application
------------------

In the graph above, it has 4 blocks, and our code will create 4 functions based on these blocks.

.. sourcecode:: python

    import time
    from flowsdk import Application
    from flowsdk.strategies import OrderedByTime
    from pylib.data.types import Value

    # initialize Application instance
    app = Application()
    index: int = 0


    @app.timer(interval=1)
    async def generator():
        """
            periodic task running every 1s
            data is sent out to c1 and c2 in round-robin, and 'value' requires to be in types.Value class
        """
        global index
    
        index += 1
        if (index % 2) == 1:
            await app.send('c1', value=Value(index=index))
        else:
            await app.send('c2', value=Value(index=index))
    

    @app.listen('c1')
    async def cm1(data):
        """
            CM1: receive the stream data from channel c1, and send out to the channel c3
                 'data()' is the types.Value, and 'data' is the  types.Packet
        """
        print(f'....CM1 received: {data.index} time at: {app.get_time()}')
        await app.send('c3', value=data)

    
    @app.listen('c2')
    async def cm2(data):
        """
            CM2: receive the stream data from channel c2, and send out to the channel c4
                 'data()' is the types.Value, and 'data' is the  types.Packet
        """
        print(f'....CM2 received: {data.index} time at: {app.get_time()}')
        time.sleep(5)
        await app.send('c4', value=data)


    @app.listen('c3', 'c4', strategy=OrderedByTime)
    async def cm3(data):
        """
            CM3: receive the stream data from channel c3, and c4 in merge mode.
        """
        print(f'$$$$CM3 received: {data.index} time at: {app.get_time()}')


    if __name__ == '__main__':
        """
            start the application main()
        """
        app.main()

We use python decorator in this code, as the data flow describes, cm1 and cm2 will get data from generator, 
cm1 listens on c1 channel and cm2 listens on c2 channel.

cm3(processor) will get data from cm1 and cm2, then process data and output. So cm3 will listen on c3 and c4 channels,
and use strategy "OrderedByTime" to process data.

We also support a coding way without using decorators. You need to specify in '__main__':

.. sourcecode:: python

    if __name__ == '__main__':
    
        app.create_stream_processor(StreamProcessor(['c1'], proc=cm1))
        app.create_stream_processor(StreamProcessor(['c2'], proc=cm2))
        app.create_stream_processor(StreamProcessor(['c3', 'c4'], proc=cm3, strategy=OrderedByTime))
        app.main()



Starting Kafka
---------------

Before running your app, you need to start Zookeeper and Kafka.

Start Zookeeper first:

.. sourcecode:: console

    $ $KAFKA_HOME/bin/zookeeper-server-start $KAFKA_HOME/etc/kafka/zookeeper.properties

Then start Kafka:

.. sourcecode:: console

    $ $KAFKA_HOME/bin/kafka-server-start $KAFKA_HOME/etc/kafka/server.properties


Running your app
------------------

.. sourcecode:: console
    
    $ python cm-agent.py