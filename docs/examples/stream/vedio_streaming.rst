.. _examples-stream-vedio_streaming:

=====================
  Vedio Streaming
=====================

This is an example to show how to stream packets of a video. The pipeline can be depicted as such.

.. sourcecode:: bash

     +-----------------+
     | produce_packets |
     +-----------------+
              | packet
              V
    +-------------------+
    | select_key_frames |
    +-------------------+
              | {stream_index, frame}
              v
       +-------------+
       | save_frames |
       +-------------+


Running
---------

1. Install FFmpeg

.. sourcecode:: bash

    brew install ffmpeg

2. Remember to set python emvironment first.

.. sourcecode:: bash

    virtualenv -p python3 venv
    source venv/bin/activate
    pip install -r requirements.txt

3. Start ZooKeeper and Kafka server:

.. sourcecode:: bash

    $ bin/zookeeper-server-start.sh config/zookeeper.properties
    $ bin/kafka-server-start.sh config/server.properties

4. Run the program to generate the key frames in the output/ directory

.. sourcecode:: bash

    python video-streaming.py worker

