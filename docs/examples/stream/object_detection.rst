.. _examples-stream-object_detection:

======================
   Object Detection
======================

Use cv2 to read vedio, make sure install cv2 first.

.. sourcecode:: bash

    pip install opencv-python


Run Object detection example:

.. sourcecode:: bash

    python read_video.py --input videos/airport.mp4

    spark-submit ./process_image_spark.py

    python display_image.py

    python send_alert.py
