.. _examples-stream:

====================
   Stream process
====================

.. toctree::
    :maxdepth: 1
    
    cm_example
    vedio_streaming


Stream processing is important and fundamental in flowsdk.

`Cm_sample` introduces strategies about how to process data when have different sources. 

`Vedio streaming` is an example to show how to use flowsdk on a vedio.

