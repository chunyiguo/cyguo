.. _examples-quickstart:

===================
   Quick Start
===================

.. contents::
    :local:
    :depth: 1

Create a simple stream processing application.

Here is an simple example of data processing:

The goal is to send data from a node to another node.

Data flow
-----------
.. sourcecode:: bash
    
    +-----------+
    |  sender   |
    +-----------+
         |
         | channel
         |
         v
    +-----------+
    |  receiver |
    +-----------+

In this flow picture, data will be transferred from sender to receiver. A channel is a queue/buffer used to send and receive messages, like a bridge between sender and receiver.
Based on data flow, create `config.json` file.

.. sourcecode:: bash
    
    {
      "id": "simple_example",
      "kafka": {
        "broker": "kafka://localhost:9092"
      },
      "channels": {
        "inputs": {
          "input": {
            "topic": "simple_example_common_topic",
            "schema": "{}"
          }
        },
        "outputs": {
          "output": {
            "topic": "simple_example_common_topic",
              "schema": "{}"
          }
        }
      }
    }

"id" : name of this application

"kafka": assign kafka broker's address, if use local kafka, it usually uses port 9092.

"channels": a channel has input and output, topic is the name of a channel



Application
------------
Now create the file `simple.py`

.. sourcecode:: python

    from flowsdk import Application
    from pylib.data.types import Value

    app = Application()


    def print_value(prefix: str, value: Value):
        print('{} {} at {}'.format(prefix, value.to_native(), app.get_time()))


    @app.timer(interval=1)
    async def send():
        value = Value(number=123.45, string='hello', map={'a': 1, 'b': True})
        print_value('Sending:  ', value)
        await app.send('output', value)


    @app.listen('input')
    async def receive(value):
        print_value('Receiving:', value)


    if __name__ == '__main__':
        app.main()

``app = Application()`` is the most important part in the above code. It defines a "stream processor" and build a channel between sender and receiver.

``Value`` is a python class defined in ``pylib`` library. Flowsdk delivers every data in Value type.
 
Decorator ``@app.listen('input')`` means this ``aysnc def receive`` function is listening to a channel named ``input`` and will print value if it received data from channel.

``app.main()`` is used to run the whole application. 

This is part of :ref:`examples`, click for more details.


Starting Kafka
---------------

Before running your app, you need to start Zookeeper and Kafka.

Start Zookeeper first:

.. sourcecode:: console

    $ $KAFKA_HOME/bin/zookeeper-server-start $KAFKA_HOME/etc/kafka/zookeeper.properties

Then start Kafka:

.. sourcecode:: console

    $ $KAFKA_HOME/bin/kafka-server-start $KAFKA_HOME/etc/kafka/server.properties


Running your app
------------------

.. sourcecode:: console
    
    $ python simple.py