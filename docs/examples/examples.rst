.. _examples:

======================
      Examples
======================


.. toctree::
    :maxdepth: 1
    
    quickstart
    stream/stream
    batch/batch


Before running code:

1. Install ``virtual environment``:
   
Every example has its own requirements.  ``requirements.txt`` is in example's folder.
    
.. sourcecode:: bash

    pip install -r requirements.txt


2. Start server:

in ``kafka_2.12-2.2.0`` directory (if not downloaded, download `here`_ ):
    
start ``zookeeper`` first:

.. sourcecode:: bash

    bin/zookeeper-server-start.sh config/zookeeper.properties  

then start ``kafka`` in a new terminal window:

.. sourcecode:: bash

    bin/kafka-server-start.sh config/server.properties



.. _`here`: https://www.apache.org/dyn/closer.cgi?path=/kafka/2.2.0/kafka_2.12-2.2.0.tgz



