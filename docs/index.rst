==================================================
Welcome to SkyDiscovery-dataflow's documentation!
==================================================

.. sourcecode:: python

    # Python  ٩(◕‿◕)۶

.. include:: includes/prerequisites_install.txt

---------------------------------------------------

Contents
========

.. toctree::
    :maxdepth: 1

    introduction


.. toctree::
    :maxdepth: 2

    examples/examples


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
